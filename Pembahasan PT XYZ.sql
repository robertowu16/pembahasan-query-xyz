create table MembershipStatus(
	MembershipStatusId int identity(1,1) primary key,
	Name varchar(255) not null, --free/premium
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Promo(
	PromoId int identity(1,1) primary key,
	PromoName varchar(255) not null,
	Description varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
) 

create table Membership(
	MembershipId int identity(1,1) primary key,
	MembershipStatusId int foreign key references MembershipStatus(MembershipStatusId),
	ValidFrom datetimeoffset,
	ValidTo datetimeoffset,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Role(
	RoleId int identity(1,1) primary key,
	Name varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Users(
	userId int identity(1,1) primary key,
	roleId int foreign key references Role(roleId),
	membershipId int foreign key references Membership(membershipId),
	username varchar(255) not null,
	email varchar(255) not null unique,
	password varchar(255) not null,
	suspendedStartAt datetimeoffset,
	suspendedEndAt datetimeoffset,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table UserPayment(
	UserPaymentId int identity(1,1) primary key,
	UserId int foreign key references Users(UserId),
	PromoId int foreign key references Promo(PromoId),
	MembershipId int foreign key  references Membership(MembershipId),
	PaidAt datetimeoffset,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

Create table NewsCategory(
	NewsCategoryId int identity(1,1) primary key,
	NewsCategoryName varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

Create table News(
	NewsId int identity(1,1) primary key,
	NewsCategoryId int foreign key references NewsCategory(NewsCategoryId),
	Description varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table NewsComment(
	NewsCommentId int identity(1,1) primary key,
	NewsId int foreign key references News(NewsId),
	UserId int foreign key references Users(UserId),
	Comment varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

Create table VideoCategory(
	VideoCategoryId int identity(1,1) primary key,
	VideoCategoryName varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)


create table Video(
	VideoId int identity(1,1) primary key,
	VideoCategoryId int foreign key references VideoCategory(VideoCategoryId),
	VideoName varchar(255) not null,
	Content varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Streaming(
	StreamingId int identity(1,1) primary key,
	VideoId int Foreign Key references Video(VideoId),
	UserId int Foreign Key references Users(UserId),
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)


create table Ads(
	AdsId int identity(1,1) primary key,
	AdsName varchar(255) not null,
	ValidFrom datetimeoffset not null,
	ValidTo datetimeoffset not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

Create Table VideoAds(
	VideoAdsId int identity(1,1) primary key,
	AdsId int foreign key references Ads(AdsId),
	VideoId int foreign key references Video(VideoId),
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table ForumCategory(
	ForumCategoryId int identity(1,1) primary key,
	ForumCategoryName varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table ForumHeader(
	ForumId int identity(1,1) primary key,
	ForumCategoryId int foreign key references ForumCategory(ForumCategoryId),
	ForumName varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table ForumDetail(
	ForumDetailId int identity(1,1) primary key,
	ForumId int foreign key references ForumHeader(ForumId),
	UserId int foreign key references Users(UserId),
	Content varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
) 


create table ReportCategory(
	ReportCategoryId int identity(1,1) primary key,
	ReportCategoryName varchar(255) not null,
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)

create table Report(
	ReportId int identity(1,1) primary key,
	ReportCategoryId int foreign key references ReportCategory(ReportCategoryId),
	Description varchar(255) not null,
	SubmitedBy int foreign key references Users(UserId) not null,
	ReviewedBy int foreign key references Users(UserId),	
	CreatedAt datetimeoffset default getdate(),
	CreatedBy varchar(255) default 'SYSTEM',	
	UpdatedAt datetimeoffset default getdate(),
	UpdatedBy varchar(255) default 'SYSTEM'
)
